# README #

Aaron's keybindings for Sublime

Add default keybindings to mac: https://bitbucket.org/aaron-feldman/keybindings

Then comment out ctrl+k and ctrl+l overrides in sublime.

```
$ mkdir -p ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/Default
$ touch: /Users/aaron/Library/Application\ Support/Sublime\ Text\ 3/Packages/Default/Default\ \(OSX\).sublime-keymap
$ subl /Users/aaron/Library/Application\ Support/Sublime\ Text\ 3/Packages/Default/Default\ \(OSX\).sublime-keymap
```
```
Then comment out lines for ctrl+k and ctrl+l
```